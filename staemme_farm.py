#!/usr/bin/python

import sqlite3
import time
from os import system

db = sqlite3.connect("db_staemme_farm.db")

try:
    db.execute('''CREATE TABLE villages
                    (ID INTEGER PRIMARY KEY AUTOINCREMENT,
                    X   INT NOT NULL,
                    Y   INT NOT NULL,
                    PLAYER  TEXT,
                    POINTS  INT,
                    LAST_ATT    REAL    NOT NULL,
                    WALL    INT,
                    BARB    INT,
                    INFO    TEXT);''')
    db.execute('''CREATE TABLE player
                    (ID INTEGER PRIMARY KEY AUTOINCREMENT,
                    NAME TEXT,
                    POINTS INT,
                    PROTECT INT);''')
    db.commit()
except:
    print("Table already created!")

user = db.execute("SELECT * FROM player;").fetchone()

if not user:
    print("First Time Setup!\n")
    player_name = input("Your Name: ")
    while True:
        try:
            player_points = int(input("Your Points: "))
        except ValueError:
            print("Needs to be a number!")
            continue
        break
    while True:
        try:
            protect_ratio = int(input("Beginners protection ratio: "))
        except ValueError:
            print("Needs to be a number!")
            continue
        break
    db.execute("INSERT INTO player (NAME, POINTS, PROTECT) \
                VALUES('" + player_name + "'," + str(player_points) + "," + str(protect_ratio) + ");")
    db.commit()
    user = db.execute("SELECT * FROM player;").fetchone()

system("clear")

def menu():
    print("Welcome to Staemme Farm.")
    print("User: %s   Points: %d   Protect Ratio: %d:1\n" % (user[1],user[2],user[3]))
    print("Choose an option:")
    print("1. Get next village to Farm")
    print("2. Add new village")
    print("3. Delete village")
    print("4. List all villages")
    print("5. Change player info")
    print("6. Quit\n")
    while True:
        try:
            user_in = int(input("[1-6]: "))
        except ValueError:
            print("Needs to be either 1, 2, 3, 4, 5 or 6!")
            continue
        if user_in not in [1,2,3,4,5,6]:
            print("Needs to be either 1, 2, 3, 4, 5 or 6!")
            continue
        break
    if user_in == 1:
        system("clear")
        get_next()
        system("clear")
        menu()
    if user_in == 2:
        system("clear")
        new_village()
        system("clear")
        menu()
    if user_in == 3:
        system("clear")
        quit()
        system("clear")
    if user_in == 4:
        system("clear")
        get_all()
        system("clear")
        menu()
    if user_in == 5:
        system("clear")
        quit()
        system("clear")
    if user_in == 6:
        system("clear")
        db.close()
        quit()

def new_village():
    print("Add a new village.\n")
    while True:
        try:
            x = int(input("x coordinate: "))
        except ValueError:
            print("Needs to be a 3 digit number!")
            continue
        if len(str(x)) != 3:
            print("Needs to be a 3 digit number!")
            continue
        break
    while True:
        try:
            y = int(input("y coordinate: "))
        except ValueError:
            print("Needs to be a 3 digit number!")
            continue
        if len(str(y)) != 3:
            print("Needs to be a 3 digit number!")
            continue
        break
    player = input("Playername: ")
    while True:
        try:
            vill_points = int(input("Points: "))
        except ValueError:
            print("Needs to be a number!")
            continue
        break
    while True:
        try:
            wall = int(input("Wall level: "))
        except ValueError:
            print("Needs to be a number!")
            continue
        break
    while True:
        try:
            barb = int(input("Is Barbarian: "))
        except ValueError:
            print("Needs to be either 0(no) or 1(yes)!")
            continue
        if barb not in [0,1]:
            print("Needs to be either 0(no) or 1(yes)!")
            continue
        break
    info = input("Additional information: ")
    db.execute("INSERT INTO villages (X,Y,PLAYER,LAST_ATT,WALL,BARB,INFO,POINTS) \
                  VALUES (" + str(x) + ", " + str(y) + ", '" + player + "', '" + str(time.time()) + "', \
                  " + str(wall) + ", " + str(barb) + ", '" + info + "'," + str(vill_points) + ");")
    db.commit()

def get_next():
    c = db.execute("SELECT * from villages order by LAST_ATT LIMIT 1")
    print("Next village to attack is:\n")
    for row in c:
        print("Location: %d|%d" % (row[1],row[2]))
        print("Player: %s" % (row[3]))
        print("Points: %d" % (row[4]))
        print("Wall Level: %d" % (row[6]))
        print("Last attack was: %s\n" % (time.asctime(time.localtime(row[5]))))
        db.execute("UPDATE villages SET LAST_ATT = " + str(time.time()) + " WHERE ID = " + str(row[0]) + ";")
        input("Press enter to continue: ")
    db.commit()

def get_all():
    c = db.execute("SELECT * from villages")
    header = "%-4s¦%-8s¦%-15s¦%-10s¦%-25s¦%-6s¦%-6s¦%-10s" % ("ID","Place","Player","Points","Last Attack","Wall","Barb","Info")
    print(header)
    print("-" * len(header))
    for row in c:
        print("%4d¦%d|%d ¦%-15s¦%10d¦%-25s¦%6d¦%6d¦%s" % (row[0],row[1],row[2],row[3],row[4],time.asctime(time.localtime(row[5])),row[6],row[7],row[8]))
    print("\n")
    input("Press enter to continue: ")

# def delete_village():

menu()
